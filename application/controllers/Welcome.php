<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('api',['https://jsonplaceholder.typicode.com']);
	}

	public function index()
	{
		$data = [
			'content' => 'content'
		];
		$this->load->view('base',$data);
	}

	public function helo()
	{
		print('<pre>'.print_r($_SESSION,true).'</pre>');die();
	}

	public function login()
	{
		$this->load->view('login');
	}

	public function coba_get()
	{
		$request = $this->api->request('GET','/posts');
		echo $request;
	}

	public function coba_post()
	{
		$param = [
			'title' => 'foo',
			'body' => 'bar',
			'userId' => 1,
		];
		$request = $this->api->request('POST','https://jsonplaceholder.typicode.com/posts',$param);
		echo $request;
	}

	public function telkom()
	{
		$client = new GuzzleHttp\Client();
		$res = $client->request('GET', 'http://assurance.telkom.co.id/proman/index.php', ['allow_redirects' => false]);
		echo $res->getStatusCode();
		$data = $res->getBody();
		echo json_encode($data);
		print('<pre>'.print_r($res,true).'</pre>');die();
	}
}
