<?php
date_default_timezone_set('Asia/Jakarta');
class Authentication extends CI_Controller
{
	var $uri;
	function __construct()
	{
		parent::__construct();
		$this->load->library('api');
		$this->load->model('UserModel');
		$this->uri = base_url('index.php/API/V1');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function register()
	{
		$param = $this->input->post();
		$request = $this->api->request('POST',$this->uri.'/auth/register',$param);
		$request = json_decode($request,true);
		if($request['status'] == 'success'){
			$this->session->set_flashdata('alert_success',$request['message']);
		}else{
			$this->session->set_flashdata('alert_failed',$request['message']);
		}
		redirect('login');
	}

	public function login()
	{
		$param = $this->input->post();
		$request = $this->api->request('POST',$this->uri.'/auth/login',$param);
		$request = json_decode($request,true);
		if($request['status'] == 'success'){
			$this->session->set_userdata('data',$request['data']);
			$data = [
				'content' => 'content'
			];
			$this->load->view('base',$data);
		}else{
			$this->session->set_flashdata('alert_failed',$request['message']);
			redirect('login');
		}
	}

	public function logout()
	{
		$this->UserModel->destroyToken($_SESSION['data']['user']['username']);
		$this->session->unset_userdata('data');
		redirect('login');
	}
}
?>