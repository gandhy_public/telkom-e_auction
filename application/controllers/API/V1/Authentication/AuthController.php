<?php
date_default_timezone_set('Asia/Jakarta');
class AuthController extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
	}

	public function register()
	{
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$type = $this->input->post('type');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$repass = $this->input->post('repass');

		try {
			if(!isset($name)) throw new Exception("Name is required", 1);
			if(!isset($email)) throw new Exception("Email is required", 1);
			if(!isset($type)) throw new Exception("Type is required", 1);
			if(!isset($username)) throw new Exception("Username is required", 1);
			if(!isset($password)) throw new Exception("Password is required", 1);
			if(!isset($repass)) throw new Exception("Repassword is required", 1);
			if(md5($password) != md5($repass)) throw new Exception("Password and repassword is not same", 1);
			
			$store = $this->UserModel->register($this->input->post());
			if($store !== TRUE) throw new Exception($store, 1);

			$resp = [
				'status' => 'success',
				'message' => 'success please login',
			];
		} catch (Exception $e) {
			$resp = [
				'status' => 'error',
				'message' => $e->getMessage(),
			];
		}

		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}

	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		try {
			if(!isset($username)) throw new Exception("Username is required", 1);
			if(!isset($password)) throw new Exception("Password is required", 1);

			$login = $this->UserModel->login($this->input->post());
			if($login['status'] === FALSE) throw new Exception($login['message'], 1);
			
			$resp = [
				'status' => 'success',
				'message' => 'success',
				'data' => $login
			];
		} catch (Exception $e) {
			$resp = [
				'status' => 'error',
				'message' => $e->getMessage(),
			];
		}

		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($resp));
	}
}
?>