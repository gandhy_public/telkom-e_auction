<?php
date_default_timezone_set('Asia/Jakarta');
class UserModel extends CI_Model
{
	
	function register($data)
	{
		try {
			$cek = $this->db->get_where('users',['username' => $data['username']])->result_array();
			if(count($cek)) throw new Exception("Username is already exists", 1);

			$insert = [
				'name' => $data['name'],
				'email' => $data['email'],
				'type' => $data['type'],
				'username' => $data['username'],
				'password' => md5($data['password']),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];
			$insert = $this->db->insert('users',$insert);
			if(!$insert) throw new Exception("Inserting data failed", 1);
			
			$resp = TRUE;
		} catch (Exception $e) {
			$resp = $e->getMessage();
		}

		return $resp;
	}

	public function login($data)
	{
		try {
			$cek = $this->db->get_where('users',['username' => $data['username'], 'password' => md5($data['password'])])->result_array();
			if(count($cek) == 0) throw new Exception("User akun not found", 1);

			$token = $this->createToken($cek[0]['username']);
			$this->db->set('token', $token)->where('id', $cek[0]['id'])->update('users');
			$user = [
				'name' => $cek[0]['name'],
				'email' => $cek[0]['email'],
				'type' => $cek[0]['type'],
				'username' => $cek[0]['username'],
			];

			$resp = [
				'status' => TRUE,
				'user' => $user,
				'token' => $token
			];			
		} catch (Exception $e) {
			$resp = [
				'status' => FALSE,
				'message' => $e->getMessage()
			];
		}

		return $resp;
	}

	public function createToken($username)
	{
		$date = date_create(date('Y-m-d H:i:s'));
		date_add($date, date_interval_create_from_date_string('30 minutes'));
		$date = date_format($date, 'Y-m-d H:i:s');
		$token = rand(100,999)."|".$username."|".$date;
		return base64_encode($token);
	}

	public function destroyToken($username)
	{
		$this->db->set('token', NULL)->where('username', $username)->update('users');	
	}
}
?>