<div class="page-content">
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Sidebar Layouts</span>
            </li>
        </ul>
    </div>
    <h1 class="page-title"> Fixed Sidebar
        <small>fixed sidebar option</small>
    </h1>
    <div class="note note-info">
        <p> To set fixed sidebar layout just apply <code>page-sidebar-fixed</code> class to the body element. </p>
    </div>
</div>