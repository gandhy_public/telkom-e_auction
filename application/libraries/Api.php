<?php
class Api
{
	var $base;
	var $client;

	function __construct()
	{
		$this->client = new GuzzleHttp\Client();
	}

	public function request($method,$uri,$param = [])
	{
		if(count($param)){
			$param = [
				'form_params' => $param
			];
		}
		$res = $this->client->request($method, $uri, $param);
		return $res->getBody();
	}
}
?>